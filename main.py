from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score

import pandas as pd
from tabulate import tabulate

NUMBER_NEIGHBORS = 3

"""
Dataset description:

SOURCE: https://www.kaggle.com/ronitf/heart-disease-uci

Attribute Information:
1. age
2. sex
3. chest pain type (4 values)
4. resting blood pressure
5. serum cholestoral in mg/dl
6. fasting blood sugar > 120 mg/dl
7. resting electrocardiographic results (values 0,1,2)
8. maximum heart rate achieved
9. exercise induced angina
10. oldpeak = ST depression induced by exercise relative to rest
11. the slope of the peak exercise ST segment
12. number of major vessels (0-3) colored by flourosopy
13. thal: 3 = normal; 6 = fixed defect; 7 = reversable defect
"""


def read_test_train_data():
    """
    Read train and test file from input file directory

    :return: train_data, test_data
    """

    input_data = pd.read_csv("./input_data/heart.csv")
    train_data, test_data = train_test_split(input_data, test_size=0.2, random_state=0)
    return train_data, test_data


def show_data(data):
    """
    Show data as a table

    :param data: data that loaded
    :return: nothing
    """
    print(tabulate(data, headers='keys', tablefmt='psql', showindex=False))
    print()


def preprocess_data(data):
    """
    Preprocess on data. Applying normalisation on some columns
    :param data: input data
    :return: preprocessed data
    """

    normalized_df = (data - data.min()) / (data.max() - data.min())
    return normalized_df


def separate_data_to_xy(data):
    """
    Separate data to x part and y part (label)

    :param data: the input data
    :return: x and y from data
    """

    x = data.iloc[:, 0:12]
    y = data.iloc[:, 13]

    return x, y


def create_knn_model(train_data):
    """
    Create a knn classifier model with sklearn

    LIBRARY: https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html

    :return: the knn model
    """

    x, y = separate_data_to_xy(train_data)

    neigh = KNeighborsClassifier(n_neighbors=NUMBER_NEIGHBORS)
    neigh.fit(x, y)
    return neigh


def create_naive_bayes_model(train_data):
    """
    Create a naive bayes model with sklearn

    LIBRARY: https://scikit-learn.org/stable/modules/naive_bayes.html

    :param train_data:
    :return: the naive bayes model
    """

    x, y = separate_data_to_xy(train_data)

    gnb = GaussianNB()
    gnb.fit(x, y)
    return gnb


def test_model(model, test_data):
    """
    Test a classifier with test data

    :param model: the classifier model
    :param test_data: test data
    :return: nothing
    """
    x, y = separate_data_to_xy(test_data)

    # Predict labels
    predicts = model.predict(x)

    # Calculate accuracy
    accuracy = accuracy_score(y, predicts)
    print(f"Accuracy = {accuracy*100}%")


def run():
    """ Run the program! """

    # Read train and test data
    train_data, test_data = read_test_train_data()

    # Preprocessing data
    train_data = preprocess_data(train_data)
    test_data = preprocess_data(test_data)

    # Show data for better sense
    show_data(train_data)

    # Create knn classifier model
    neight = create_knn_model(train_data)

    # Create naive bayes classifier model
    gnb = create_naive_bayes_model(train_data)

    # Test model with test data
    print("Naive bayes classifier ", end='')
    test_model(gnb, test_data)
    print("KNN classifier ", end='')
    test_model(neight, test_data)


if __name__ == '__main__':
    run()
